package pl.spio.ecommerce.a;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;


public class Digest {

	/**
	 * @param args
	 * @throws NoSuchAlgorithmException 
	 * @throws UnsupportedEncodingException 
	 */
	public static void main(String[] args) throws NoSuchAlgorithmException, UnsupportedEncodingException {
		// TODO Auto-generated method stub

		String password ="test";
	
		MessageDigest digest = MessageDigest.getInstance("SHA-256");
		digest.update(password.getBytes("utf-8"));
		byte[] hash = digest.digest();
		
		System.out.println(org.apache.commons.codec.binary.Base64.encodeBase64String(hash));
		
	}

}
