package pl.spio.ecommerce.account;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

@Stateless
public class UserControllerBean implements IUserControllerBean {
	@PersistenceContext(unitName = "postgres")
	private EntityManager manager;

	@Override
	public User findUser(String username) {
		TypedQuery<User> query = manager.createNamedQuery(
				"User.findByUsername", User.class).setParameter("username",
				username);
		return query.getSingleResult();
	}
	
	
	public User findUserByID(long userId) {
		TypedQuery<User> query = manager.createNamedQuery(
				"User.findUserById", User.class).setParameter("id",
				userId);
		return query.getSingleResult();
	}

	@Override
	public List<User> getAllUsers() {
		TypedQuery<User> query = manager.createNamedQuery("User.findAll", User.class);
		return query.getResultList();
	}

	@Override
	public void removeUser(long userIdToRemove) {
		TypedQuery<User> query = manager.createNamedQuery(
				"User.deleteUser", User.class).setParameter("id",
						userIdToRemove);
		query.executeUpdate();
	}
}