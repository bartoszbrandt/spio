package pl.spio.menu;

import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;

@ManagedBean
@ApplicationScoped
public class menuBean {
	public String userManagement() {
		return "/management/users.xhtml";
	}

	public String productManagement() {
		return "/management/products.xhtml";
	}
}