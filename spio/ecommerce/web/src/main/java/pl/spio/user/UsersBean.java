package pl.spio.user;

import java.util.List;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import pl.spio.ecommerce.account.User;
import pl.spio.ecommerce.account.IUserControllerBean;

@ManagedBean
@ViewScoped
public class UsersBean {
	@EJB
	private IUserControllerBean userControllerBean;
	private long userIdToRemove;
	private long userIdToEdit;
	

	public List<User> getAllUsers() {
		return userControllerBean.getAllUsers();
	}
	
	public User getUser() {
		return userControllerBean.findUserByID(userIdToEdit);
	}

	public void removeUser() {
		userControllerBean.removeUser(userIdToRemove);
	}

	public long getUserIdToRemove() {
		return userIdToRemove;
	}

	public void setUserIdToRemove(long userIdToRemove) {
		this.userIdToRemove = userIdToRemove;
	}
	
	public void setUserIdToEdit(long userIdToEdit) {
		this.userIdToEdit = userIdToEdit;
	}
}