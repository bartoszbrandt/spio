package pl.spio.ecommerce.account;

import java.util.List;


public interface IUserControllerBean {

	public abstract User findUser(String username);
	public abstract User findUserByID(long userId);

	public abstract List<User> getAllUsers();

	public abstract void removeUser(long userIdToRemove);

}