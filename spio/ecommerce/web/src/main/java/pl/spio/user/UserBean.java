package pl.spio.user;

import javax.annotation.ManagedBean;
import javax.ejb.EJB;
import javax.faces.bean.ViewScoped;

import pl.spio.ecommerce.account.IUserControllerBean;
import pl.spio.ecommerce.account.User;

@ManagedBean
@ViewScoped
public class UserBean {
	
	@EJB
	private IUserControllerBean userControllerBean;
	private User user;
	private long userId;

	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}
	
	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public void initUser()
	{
		user = userControllerBean.findUserByID(userId);
	}
	
}
